;;; rename all predicates to scheme-like predicates
;;; atom? null? cons? even? odd? ...
;;; rename remove-if remove-if-not to be modern
;;; filter select ...

(defpackage #:scheme-ish
  (:use #:cl #:nolambda #:str))

(in-package :scheme-ish)


(defparameter names '((filter remove-if)
                      (select remove-if-not)
                      (null? null)
                      (atom? atom)))

(defparameter old-names (mapcar #'cadr names))
(defparameter new-names (mapcar #'car  names))
(defparameter not-valid '(gentemp pprint-pop map exp sleep remprop pop step loop))

(defun get-preds (cl-symbols)
  "Find all words that end in -p or end in p but not found in 'not-valid"
  (mapcar (lambda (s)
            (when (not (member s not-valid))
              (let* ((sname  (symbol-name s))
                     (splits (str::split #\- sname))
                     (tail   (first (last splits)))
                     (head   (butlast splits)))
                (list (cond ((equalp "p" tail)
                             (intern (format nil "~a?" (str::join #\- head)) *package*))
                            ((equalp "p" (str::s-last (symbol-name s)))
                             (intern (format nil "~{~a~}?" (butlast (coerce sname 'list))) *package*)))
                      s))))
          cl-symbols))


(defun rename (names)
  "given a list of names ((new old) (new old) ...) set the new name to the old value"
  (flet ((rn (pair)
               (let ((new (first  pair))
                     (old (second pair)))
                 (when (fboundp old)
                   (setf (symbol-function new)
                         (symbol-function old))))))
        (dolist (pair names)
                (rn pair)))
  names)


;;; having collected all of the symbols, rename them all
;;; then remove all the old names
;;; export the results
(let (symbols)
  (do-external-symbols (s (find-package 'cl))
                       (push s symbols))
  (export
   (let* ((all-names  (append (rename names)
                              (rename (remove-if (lambda (x) (null (first x))) (get-preds symbols)))))
          (old-names (mapcar #'second all-names))
          (new-names (mapcar #'first  all-names)))
     (append new-names
             (filter {member x old-names} symbols)))))
